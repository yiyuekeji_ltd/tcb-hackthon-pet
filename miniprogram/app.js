//app.js
import CONFIG from './config/config'
App({
  onLaunch: function () {
    //console.log(CONFIG)
    if (!wx.cloud) {
      console.error('请使用 2.2.3 或以上的基础库以使用云能力')
      wx.showToast({
        icon: 'none',
        title: '请更新微信版本',
        duration: 3000,
      })
    } else {
      wx.cloud.init({
        env: CONFIG.CLOUDCONFIG.CLOUDENV,
        traceUser: true,
      })
    }
    this.globalData = { version: CONFIG.APPCONFIG.VERSION, location: CONFIG.MAPCONFIG.DEFLOCATION };
    wx.cloud.callFunction({
      name: 'user',
      data: { action: 'login' },
      success: res => {
        console.log('[云函数] [login] userInfo: ', res.result)
        this.globalData.userInfo = res.result;
      },
      fail: err => {
        console.error('[云函数] [login] 调用失败', err)
      }
    })
    wx.getSystemInfo({
      success: systemInfo => {
        this.globalData.StatusBar = systemInfo.statusBarHeight;
        let capsule = wx.getMenuButtonBoundingClientRect();
        if (capsule) {
          this.globalData.Custom = capsule;
          this.globalData.CustomBar = capsule.bottom + capsule.top - systemInfo.statusBarHeight;
        } else {
          this.globalData.CustomBar = systemInfo.statusBarHeight + 50;
        }
        if (systemInfo.brand != "devtools") {
          const db = wx.cloud.database();
          db.collection('devlog').add({
            data: systemInfo
          })
        }
      }
    })
  }
})
